from flask import Flask, request, jsonify
from flask_cors import CORS
import requests
from sentence_transformers import SentenceTransformer, util
import json
import dotenv
import os

app = Flask(__name__)
CORS(app)

model_name = 'all-distilroberta-v1'
model = SentenceTransformer(model_name)

dotenv.load_dotenv()

api_key = os.environ.get("AP_KEY")
backend_url_prefix = os.environ.get("BACKEND_URL")

def find_synonyms(word):
    url = f'https://words.bighugelabs.com/api/2/{api_key}/{word}/json'
    response = requests.get(url)
    synonyms = []
    if response.status_code == 200 and 'noun' in response.json():
        synonyms += response.json()['noun']['syn']
    return synonyms

def calculate_similarity(word1, word2):
    word_embeddings = model.encode([word1, word2], convert_to_tensor=True)
    word1_embedding = word_embeddings[0]
    word2_embedding = word_embeddings[1]
    cosine_scores = util.cos_sim(word1_embedding, word2_embedding)
    return cosine_scores

def rank_synonyms(word, synonyms):
    ranked_synonyms = sorted(synonyms, key=lambda s: calculate_similarity(s, word), reverse=True)
    return ranked_synonyms

def add_to_json(key, value, json_data=None):
    if json_data is None:
        json_data = {}
    if key not in json_data:
        json_data[key] = []
    json_data[key].append(value)
    return json_data
    
#classes = ['Paper', 'Comparison', 'Problem', 'ResearchField', 'Author', 'Resource', 'Property']

@app.route('/search', methods=['GET'])
def search():
    term = request.args.get('term')
    class_name = request.args.get('class')
    if term is None:
        return jsonify({'error': 'No search term provided.'}), 400
    if class_name is None:
        return jsonify({'error': 'No class name provided.'}), 400

    ranked_synonyms = []
    if term:
        term_synonyms = find_synonyms(term)
        if term_synonyms:
            ranked_synonyms = rank_synonyms(term, term_synonyms)
    ranked_synonyms.insert(0, term)
    responses = {}
    class_responses = {}
    for term in ranked_synonyms:
        backendUrl = f'http://{backend_url_prefix}/api/classes/{class_name}/resources/?desc=true&exact=false&page=0&q={term}'
        response = requests.get(backendUrl)
        class_responses[term] = json.loads(response.text)
    responses[class_name] = class_responses

    resultJson = {}
    contents_by_class = {}
    contents_by_class[class_name] = []
    for term in ranked_synonyms:
        if len(responses[class_name][term]['content']) > 0:
            contents_by_class[class_name].extend(responses[class_name][term]['content'])
    if len(contents_by_class[class_name]) > 0:
        unique_contents = []
        content_ids = set()
        for content in contents_by_class[class_name]:
            if content['id'] not in content_ids:
                unique_contents.append(content)
                content_ids.add(content['id'])
        resultJson["content"] = unique_contents

    if len(resultJson) == 0:
        return jsonify({'error': 'No results found.'}), 200

    return json.dumps(resultJson), 200


@app.route('/ping')
def ping():
    return json.dumps("this is ping"), 200

if __name__ == '__main__':
    app.run(debug=True)

#http://127.0.0.1:5000/search?term=result?class=
#http://localhost:8080/api/classes/ResearchField/resources/?desc=true&exact=false&page=0&q=ta&size=10&sort=id%2Cdesc
#http://sandbox.orkg.org