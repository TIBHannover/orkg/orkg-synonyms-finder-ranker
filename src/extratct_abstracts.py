import csv

csvFile_Name = '../dataset/orkg_papers.csv'
txtFile_Name = '../dataset/orkg_abstracts.txt'
with open(csvFile_Name, 'r') as csv_file:

    csv_reader = csv.reader(csv_file)

    next(csv_reader)

    for row in csv_reader:

        processed_abstract = row[5]
        if len(processed_abstract) > 5:
            with open(txtFile_Name, 'a') as text_file:
                text_file.write(processed_abstract + '\n')