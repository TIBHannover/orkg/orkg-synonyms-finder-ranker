# ORKG Synonyms Finder-Ranker
**Finding and Ranking Synonyms and Acronyms in ORKG**
<br>
Bachelorarbeit repo of Ahmed Kadri

# Overview
A search system is a software program designed to help users to look for information using specific keywords or phrases. However, the system may miss some relevant results if it fails to consider synonyms or alternative terms that may be related to the search query. This issue is more critical in a scholarly context where researchers may use different terms for searching scholarly platforms i.e. search in ORKG. To this end, finding the synonyms of query terms and ranking synonyms in the context of scholarly publications helps in finding accurate answers to the queries. The Open Research Knowledge Graph (ORKG) is a tool for representing, curating, and exploring scholarly knowledge that contains structured forms of data within the semantically rich interlinked properties, which offers a search service for the users. To offer a user-friendly search experience for the ORKG platform, we propose an approach for searching scientific terms in indexed publications in the ORKG platform for the academic community. We introduced a two-step approach that first, discovers the synonyms of the queries by utilizing an external API as a synonyms retrieval phase. Next, in the synonyms ranking phase, we used language models to semantically rank the synonyms by looking at the semantical aspect of terms in the ORKG. We targeted the evaluations based on consideration of the ORKG context by creating a testing set in a manual manner that consist of query terms and ranked synonyms according to the ORKG articles. Evaluations using various metrics show that the proposed methodology improves the search service in ORKG. This thesis highlights the significance of improving the search service in ORKG and the advantages of employing synonyms and language models. Finally, this approach can effectively enhance ORKG's search service and assist users more efficiently in retrieving relevant scholarly knowledge they are browsing.

## Results

**Experimental Results**:

| Model                          |  all-distilroberta-v1    | all-MiniLM-L6-v2     | nli-distilroberta-base-v2   | all-mpnet-base-v2-all
| ------------------------------ | ------------------------ | ------------------ | ------------------------- | ------------------------ |
|NDCG Score | 0.5742 | 0.5371 | 0.39 | 0.4918|
| Kendall Tau's score | 0.1888 | 0.1575 | 0.1589 | 0.1573|


**Training LM results***:

| Model                          |  all-distilroberta-v1    | all-MiniLM-L6-v2     | nli-distilroberta-base-v2   | all-mpnet-base-v2-all
| ------------------------------ | ------------------------ | ------------------ | ------------------------- | ------------------------ |
| NDCG score before fine-tunging | 0.5742                   |             0.5371 |       0.39                | 0.49|
| NDCG score after fine-tunging  | 0.4273                   |             0.27.   |       ****                 | ****

## How to Run

**Prerequisites**
- Python version ^3.8

Running the flask app with the following commands.
```
git clone https://gitlab.com/TIBHannover/orkg/orkg-synonyms-finder-ranker.git
cd orkg-synonyms-finder-ranker
pip install -r requirements.txt

python src/app.py
```
Before running `src/app.py` it is required to provide `API_Key` inside `.env` file similar to `.env-example` file.

###  How to test the service,
you can make a GET request to the following URL:
> http://127.0.0.1:5000/search?term=(searchTerm)&class=(ClassName)<br />
- Replace `(searchTerm)` with the specific term you are looking for
- Replace `(ClassName)` with one of the following options:
```json lines
Paper 
Comparison
Problem
ResearchField
Author
Resource
Property
```

**Examples:** 
<br>
**# 1**
<br>
If you want to search for the term `virus` within the `Paper` class, the URL would be:<br />
> http://127.0.0.1:5000/search?term=virus&class=Paper


**# 2**
<br>
If you want to search for the term  `consequenceg` within the `Comparison` class, the URL would be:<br />
> http://127.0.0.1:5000/search?term=consequence&class=Comparison


**Make sure to URL-encode any special characters in the search term if necessary.**


## Contribution
This service is developed and maintained by:

- Ahmed Kadri <ahmed.kadri@stud.uni-hannover.de>
- Golsa Heidari <golsa.heidari@tib.eu>

## Authors and acknowledgment
**Student:** Ahmed Kadri
<br>
**First evaluator:** Prof. Dr. Sören Auer
<br>
**Second evaluator:** Dr. Markus Stocker
<br>
**Supervisor:** Golsa Heidari
<br>

## Environment Variables
The following environment variables can be used inside the docker container and are defined in the .env file.

|Variable|Description|
|:---:|:---:|
|API_Key| API key from https://words.bighugelabs.com/ for synonym retreival|
|BACKEND_URL| backend url prefix, for testing it is `sandbox.orkg.org` for production it should be `orkg.org`|

## License
[MIT](./LICENSE)

