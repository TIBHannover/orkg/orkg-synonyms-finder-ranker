
# Use an official Python runtime as the base image
FROM python:3.8-slim-buster

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements.txt file and install the Python dependencies
COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

# Copy the Python script to the working directory
COPY src/app.py .
COPY .env .

# Set the entry point command to run the Flask app
CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0"]


